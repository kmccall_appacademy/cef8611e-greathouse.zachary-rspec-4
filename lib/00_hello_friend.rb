class Friend
  def greeting(someone= "")
    if someone == ""
      "Hello!"
    else
      "Hello, #{someone}!"
    end
  end
end
