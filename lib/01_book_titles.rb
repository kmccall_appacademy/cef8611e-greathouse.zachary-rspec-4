class Book
  attr_reader :title

  def title=(title)
    little_words = %w(a an and in of the)

    title_words = title.split(" ")

    title_words.each_with_index do |word, i|
      if (i == 0) && (little_words.include? word)
        word.capitalize!
      elsif (i != 0) && (little_words.include? word)
        word.downcase!
      else
        word.capitalize!
      end
    end

    @title = title_words.join(" ")
  end
end
