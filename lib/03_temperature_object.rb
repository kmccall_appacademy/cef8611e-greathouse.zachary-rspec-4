class Temperature
  def initialize(options)
    @fahrenheit = options[:f]
    @celsius = options[:c]
  end

  def self.from_fahrenheit(temp)
    self.new(f: temp)
  end

  def self.from_celsius(temp)
    self.new(c: temp)
  end

  #performs calculations to convert fahrenheit to celcius
  def self.ftoc(temperature)
    (temperature - 32) * 5 / 9.0
  end

  #performs calculations to convert celcius to fahrenheit
  def self.ctof(temperature)
    temperature * 9 / 5.0 + 32
  end

  def in_fahrenheit
    @fahrenheit.nil? ? self.class.ctof(@celsius) : @fahrenheit
  end

  def in_celsius
    @celsius.nil? ? self.class.ftoc(@fahrenheit) : @celsius
  end

end

#subclasses
class Fahrenheit < Temperature
  def initialize(temp)
    @fahrenheit = temp
  end
end

class Celsius < Temperature
  def initialize(temp)
    @celsius = temp
  end
end
